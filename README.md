[pt-br] Esse projeto pretende criar um plugin para o [TiddlyWiki5](http://www.tiddlywiki.com),
	de forma que ele possa interagir com um repositório [git](http://www.git-scm.com/)
	através da [API do gitLab](https://gitlab.com/help/api/README.md). Dessa forma,
	poderia-se ter serviços semelhantes ao [git hub pages](https://pages.github.com/) para
	[Cadernos de Pesquisas Abertos](https://en.wikipedia.org/wiki/Open_notebook_science).
